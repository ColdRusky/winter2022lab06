public class Board {

//3 private fields
private Die dice1;
private Die dice2;
private boolean[] closedTiles;

//Constructor with no input that initializes the fields
public Board() {
	
	this.dice1 = new Die();
	this.dice2 = new Die();
	this.closedTiles = new boolean[12];
	
}

//toString method to return a representation of closedTiles array
public String toString() {
	
	String value = "";
	
	//A loop to loop through the array and to assign "X" if true
	for (int i = 0; i < closedTiles.length; i++){
	
	if(closedTiles[i] == false) {
		
		value = value + " " + (i+1);
	}
	else {
		
		value = value + " X";
	}
}
return value;
}

//playATurn instance method that plays a turn for one player
public boolean playATurn() {
	
	//Calling the roll() method on both Die objects
	dice1.roll();
	dice2.roll();
	
	//Printing out the two Die objects
	System.out.println(dice1);
	System.out.println(dice2);
	
	//Equation to calculate the sum of the pips of the two Die objects
	int sum = dice1.getPips() + dice2.getPips();
	System.out.println("Sum of pips from the two die: "+ sum);
	
	//Checking the closedTiles array to see if that tile is shut or not
	if(closedTiles[sum-1] == true) {
		
		closedTiles[sum-1] = true;
		System.out.println("The tile at this position is already shut!");
		return false;
	}
	else {
		
		closedTiles[sum-1] = true;
		System.out.println("Closing tile: "+ sum);
		return true;
	}
}
}