public class ShutTheBox {


public static void main(String[] args){
	
	//Printing out a welcome message
	System.out.println("Welcome to the dice game called Shut The Box!!!");
	
	//Creating a board object 
	Board game = new Board();
	
	//Creating a boolean variable and initializing it to false
	boolean gameOver = false;
	
	//A loop that repeats while gameOver is not true
	while(!gameOver) {
		
		//PLayer 1 turn
		System.out.println("Player 1's turn : ");
		System.out.println(game);
		boolean player1 = game.playATurn();
		
		//PLayer 2 turn
		if(player1 == true) {
   System.out.println("Player 2's turn : ");
   System.out.println(game);
   boolean player2 = game.playATurn();
   
   //If player 2 tile was already shut then player 1 wins
   if(player2 == false) {
	   
	   gameOver = true;
	   System.out.println(game);
	   gameOver = true;
	   System.out.println("Player 1 wins!");

	}
}

//else if player 1 tile was already shut then player 2 wins
else {
	System.out.println(game);
	gameOver = true;
	System.out.println("Player 2 wins!");
}
	}
}
}