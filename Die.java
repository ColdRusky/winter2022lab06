import java.util.Random;

public class Die {

//2 private fields
private int pips;
private Random random;

//Constructor with no input
public Die() {
	
	this.pips = 1;
	random = new Random();
}

//Get method
public int getPips() {
	
	return this.pips;
}

//Roll method to set random values to pips in the range of 1 to 6
public int roll() {
	
	return this.pips = random.nextInt(6)+ 1;
}

//toString method to return the value of the pips
public String toString() {
	
	return "Rolled a die to a side of "+ this.pips +" pips!";
}
}